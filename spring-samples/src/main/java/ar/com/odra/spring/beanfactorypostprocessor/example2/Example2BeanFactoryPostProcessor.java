/**
 * 
 */
package ar.com.odra.spring.beanfactorypostprocessor.example2;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

import ar.com.odra.spring.beanfactorypostprocessor.example2.bean.BeanService;

/**
 * @author c.gusala
 * 
 */
@Component
public class Example2BeanFactoryPostProcessor implements BeanFactoryPostProcessor {

	private static Logger	LOGGER	= Logger.getLogger(Example2BeanFactoryPostProcessor.class);

	public void postProcessBeanFactory(ConfigurableListableBeanFactory factory) throws BeansException {
		LOGGER.info("Registering bean service as singleton.");
		BeanService beanService = new BeanService();
		beanService.setServiceName("myService");
		factory.registerSingleton("beanService", beanService);
	}
}
