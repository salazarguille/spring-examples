/**
 * 
 */
package ar.com.odra.spring.beanfactorypostprocessor;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import ar.com.odra.spring.beanpostprocessor.example5.bean.Service1;

@ContextConfiguration(locations = { "classpath:example5/app-context.xml" })
public class Example5BeanFactoryTest extends OdraSpringBaseTest {

	@Autowired
	private Service1 service1;
	
	@Test
	public void testBeansSizeQuantity() {
		assertNotNull("Beans size must be more than 0.", this.service1);
		assertTrue("Service must have logger instance inyected", this.service1.hasLogger());
	}
}
