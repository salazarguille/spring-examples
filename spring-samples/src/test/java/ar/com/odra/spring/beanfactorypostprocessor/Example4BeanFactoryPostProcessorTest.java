/**
 * 
 */
package ar.com.odra.spring.beanfactorypostprocessor;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import ar.com.odra.spring.beanfactorypostprocessor.example4.bean.Bean4;

@ContextConfiguration(locations = { "classpath:example4/app-context.xml" })
public class Example4BeanFactoryPostProcessorTest extends OdraSpringBaseTest{

	@Autowired(required = false)
	private Bean4 bean4;
	
	@Test
	public void testInjectBean4() {
		assertNotNull("Bean4 must be not null.", bean4);
	}

}
