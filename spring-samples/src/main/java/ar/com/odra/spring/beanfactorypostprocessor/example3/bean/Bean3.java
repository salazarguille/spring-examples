package ar.com.odra.spring.beanfactorypostprocessor.example3.bean;

public class Bean3 {

	private String property1;
	private int property2;
	/**
	 * @return the property1
	 */
	public String getProperty1() {
		return property1;
	}
	/**
	 * @param property1 the property1 to set
	 */
	public void setProperty1(String property1) {
		this.property1 = property1;
	}
	/**
	 * @return the property2
	 */
	public int getProperty2() {
		return property2;
	}
	/**
	 * @param property2 the property2 to set
	 */
	public void setProperty2(int property2) {
		this.property2 = property2;
	}
	
	
}
