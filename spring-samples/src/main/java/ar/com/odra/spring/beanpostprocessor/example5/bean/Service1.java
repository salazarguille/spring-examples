package ar.com.odra.spring.beanpostprocessor.example5.bean;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import ar.com.odra.spring.beanpostprocessor.example5.annotation.Log;

@Component
public class Service1 {

	@Log
	private static Logger LOGGER;
	
	public boolean hasLogger() {
		return LOGGER != null;
	}
}
