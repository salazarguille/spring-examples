/**
 * 
 */
package ar.com.odra.spring.beanfactorypostprocessor.example2.bean;

import org.apache.log4j.Logger;

/**
 * @author c.gusala
 *
 */
public class BeanService {

	private static Logger	LOGGER	= Logger.getLogger(BeanService.class);
	
	private String serviceName;
	
	public void doSomething() {
		LOGGER.info("Doing something.");
	}

	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * @param serviceName the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
}
