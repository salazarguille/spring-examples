/**
 * 
 */
package ar.com.odra.spring.beanfactorypostprocessor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import ar.com.odra.spring.beanfactorypostprocessor.example3.bean.Bean3;

@ContextConfiguration(locations = { "classpath:example3/app-context.xml" })
public class Example3BeanFactoryPostProcessorTest extends OdraSpringBaseTest{

	@Autowired(required = false)
	private Bean3 bean3;
	
	@Test
	public void testAutowireBean3SingletonInstance() {
		assertNotNull("Beans size must be more than 0.", bean3);
	}
	
	@Test
	public void testBean3PropertiesInstance() {
		assertEquals("Property must be equals.", "property1value", bean3.getProperty1());
		assertEquals("Property must be equals.", 2, bean3.getProperty2());
	}
	
}
