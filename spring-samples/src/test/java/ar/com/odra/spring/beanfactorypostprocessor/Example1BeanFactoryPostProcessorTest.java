/**
 * 
 */
package ar.com.odra.spring.beanfactorypostprocessor;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import ar.com.odra.spring.beanfactorypostprocessor.example1.Example1BeanFactoryPostProcessor;

@ContextConfiguration(locations = { "classpath:example1/app-context.xml" })
public class Example1BeanFactoryPostProcessorTest extends OdraSpringBaseTest {

	@Autowired
	private Example1BeanFactoryPostProcessor example1BeanFactoryPostProcessor;
	
	@Test
	public void testBeansSizeQuantity() {
		assertTrue("Beans size must be more than 0.", example1BeanFactoryPostProcessor.getBeansSize() > 0);
	}
}
