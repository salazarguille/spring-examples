/**
 * 
 */
package ar.com.odra.spring.beanfactorypostprocessor.example3;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

import ar.com.odra.spring.beanfactorypostprocessor.example3.bean.Bean3;

/**
 * @author c.gusala
 * 
 */
@Component
public class Example3BeanFactoryPostProcessor implements BeanFactoryPostProcessor {

	private static Logger	LOGGER	= Logger.getLogger(Example3BeanFactoryPostProcessor.class);

	public void postProcessBeanFactory(ConfigurableListableBeanFactory factory) throws BeansException {
		LOGGER.info("Modifying bean 3 properties instance.");
		Bean3 bean3 = factory.getBean(Bean3.class);
		bean3.setProperty1("property1value");
		bean3.setProperty2(2);
	}
}
