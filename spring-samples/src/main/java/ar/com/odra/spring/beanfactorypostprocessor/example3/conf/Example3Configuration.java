/**
 * 
 */
package ar.com.odra.spring.beanfactorypostprocessor.example3.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ar.com.odra.spring.beanfactorypostprocessor.example3.bean.Bean3;

/**
 * @author c.gusala
 *
 */
@Configuration
public class Example3Configuration {

	@Bean
	public Bean3 bean3() {
		return new Bean3();
	}
}
