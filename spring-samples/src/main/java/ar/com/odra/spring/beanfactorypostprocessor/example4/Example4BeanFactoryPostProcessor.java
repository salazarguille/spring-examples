/**
 * 
 */
package ar.com.odra.spring.beanfactorypostprocessor.example4;

import java.lang.reflect.Proxy;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import ar.com.odra.spring.beanfactorypostprocessor.example4.annotation.AutoInject;
import ar.com.odra.spring.beanfactorypostprocessor.example4.bean.Bean4;

/**
 * @author c.gusala
 * 
 */
@Component
public class Example4BeanFactoryPostProcessor implements BeanFactoryPostProcessor {

	private static Logger	LOGGER	= Logger.getLogger(Example4BeanFactoryPostProcessor.class);

	public void postProcessBeanFactory(ConfigurableListableBeanFactory factory) throws BeansException {
		LOGGER.info("Verifing auto inject configuration instance.");
		Map<String, Object> beans = factory.getBeansWithAnnotation(Configuration.class);
		for ( Map.Entry<String, Object> keyValue : beans.entrySet()) {
			LOGGER.info("Testing configurationo instance [" + keyValue.getKey() + "].");
			Object configurationInstance = getSourceObject(keyValue.getValue());
			if( configurationInstance != null ) {
				Class<?> configurationInstanceClass = configurationInstance.getClass();
				registerSingleton(factory, configurationInstanceClass);
			} else {
				if (Enhancer.isEnhanced(keyValue.getValue().getClass())) {
					Class<?> configurationInstanceClass = keyValue.getValue().getClass().getSuperclass();
					registerSingleton(factory, configurationInstanceClass);
				}
			}
		}
	}

	/**
	 * @param factory
	 * @param configurationInstanceClass
	 */
	private void registerSingleton(ConfigurableListableBeanFactory factory, Class<?> configurationInstanceClass) {
		if(configurationInstanceClass.isAnnotationPresent(AutoInject.class)) {
			if(!factory.containsBean("bean4")) {
				Bean4 bean4 = this.createBean4Instance();
				factory.registerSingleton("bean4", bean4);
			}
		}
	}

	protected Bean4 createBean4Instance() {
		Bean4 bean4 = new Bean4();
		bean4.setProperty1("value1");
		bean4.setProperty2(22);
		return bean4;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getSourceObject(Object proxyInstance) {
		Class<?> beanClass = proxyInstance.getClass();
		Object beanSource = null;
		if (AopUtils.isJdkDynamicProxy(proxyInstance)) {
			try {
				beanSource = ((Advised) proxyInstance).getTargetSource().getTarget();
			} catch (Exception e) {
				LOGGER.warn("Cannot get source bean in jdk dynamic proxy [" + beanClass + "].", e);
			}
		} else {
			if(AopUtils.isCglibProxy(proxyInstance)) {
				try {
					beanSource = ((Advised) proxyInstance).getTargetSource().getTarget();
				} catch (Exception e) {
					LOGGER.warn("Cannot get source bean in cglib proxy [" + beanClass + "].", e);
				}
			} else {
				if(AopUtils.isAopProxy(proxyInstance)) {
					try {
						beanSource = ((Advised) proxyInstance).getTargetSource().getTarget();
					} catch (Exception e) {
						LOGGER.warn("Cannot get source bean in aop proxy [" + beanClass + "].", e);
					}
				} else {
					if (Proxy.isProxyClass(beanClass)) {
						// return (T) proxy; // expected to be cglib proxy then, which is simply a specialized class
						LOGGER.warn("Cannot get source bean in proxy [" + beanClass + "].");
					} else {
					}
				}
			}
		}
		return (T) beanSource;
	}
}
