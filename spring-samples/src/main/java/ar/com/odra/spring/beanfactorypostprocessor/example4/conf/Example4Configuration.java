/**
 * 
 */
package ar.com.odra.spring.beanfactorypostprocessor.example4.conf;

import org.springframework.context.annotation.Configuration;

import ar.com.odra.spring.beanfactorypostprocessor.example4.annotation.AutoInject;

/**
 * @author c.gusala
 *
 */
@Configuration
@AutoInject
public class Example4Configuration {

}
