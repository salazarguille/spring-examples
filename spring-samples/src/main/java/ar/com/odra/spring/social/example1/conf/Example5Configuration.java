/**
 * 
 */
package ar.com.odra.spring.social.example1.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.social.config.support.InMemoryConnectionRepositoryConfigSupport;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.mem.InMemoryUsersConnectionRepository;
import org.springframework.social.connect.support.ConnectionFactoryRegistry;
import org.springframework.social.linkedin.api.LinkedIn;
import org.springframework.social.linkedin.api.impl.LinkedInTemplate;
import org.springframework.social.linkedin.connect.LinkedInConnectionFactory;

/**
 * @author c.gusala
 * 
 */
@Configuration
public class Example5Configuration {

	@Autowired
	private Environment environment;

	@Bean
	public ConnectionFactoryLocator connectionFactoryLocator() {
		ConnectionFactoryRegistry registry = new ConnectionFactoryRegistry();
		registry.addConnectionFactory(new LinkedInConnectionFactory(environment
				.getProperty("linkedin.consumerKey"), environment
				.getProperty("linkedin.consumerSecret")));
		return registry;
	}
	/*@Bean
	public ConnectionFactoryLocator connectionFactoryLocator() {
		ConnectionFactoryRegistry registry = new ConnectionFactoryRegistry();
		registry.addConnectionFactory(new LinkedInConnectionFactory(environment
				.getProperty("linkedin.consumerKey"), environment
				.getProperty("linkedin.consumerSecret")));
		return registry;
	}
	
	
	
	@Bean
	public UsersConnectionRepository usersConnectionRepository() {
		return new InMemoryUsersConnectionRepository(connectionFactoryLocator());
	}*/
	@Bean
	public UsersConnectionRepository usersConnectionRepository() {
		return new InMemoryUsersConnectionRepository(connectionFactoryLocator());
	}
	@Bean
	public LinkedIn linkedIn() {
		String consumerKey = "..."; // The application's consumer key
		String consumerSecret = "..."; // The application's consumer secret
		String accessToken = "3d8463b5-1ef6-4dc6-866b-bf9577f9de09"; // The access token granted after OAuth authorization
		String accessTokenSecret = "..."; // The access token secret granted after OAuth authorization
		return new LinkedInTemplate(accessToken);
	}
	/*@Bean
	public InMemoryConnectionRepositoryConfigSupport connectionRepository() {
		
		return new InMemoryConnectionRepositoryConfigSupport() {
		};
	}*/
}
