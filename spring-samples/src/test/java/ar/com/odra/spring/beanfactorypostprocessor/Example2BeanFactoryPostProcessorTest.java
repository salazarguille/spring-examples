/**
 * 
 */
package ar.com.odra.spring.beanfactorypostprocessor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;

import ar.com.odra.spring.beanfactorypostprocessor.example2.bean.BeanService;

@ContextConfiguration(locations = { "classpath:example2/app-context.xml" })
public class Example2BeanFactoryPostProcessorTest extends OdraSpringBaseTest{

	@Autowired
	private ApplicationContext applicationContext;
	
	@Test
	public void testContainsBeanServiceInContext() {
		assertTrue("Beans size must be more than 0.", applicationContext.containsBean("beanService"));
	}
	
	@Test
	public void testBeanServiceScopeSingleton() {
		BeanService beanService1 = applicationContext.getBean(BeanService.class);
		BeanService beanService2 = applicationContext.getBean(BeanService.class);
		assertNotNull("Bean instance must be not null.", beanService1);
		assertNotNull("Bean instance must be not null.", beanService2);
		assertTrue("Bean instances must be equals.", beanService1 == beanService2);
	}
	
	@Test
	public void testBeanServiceSetPropertyValue() {
		BeanService beanService1 = applicationContext.getBean(BeanService.class);
		assertEquals("Bean instance name must be equals.", "myService", beanService1.getServiceName());
	}
}
