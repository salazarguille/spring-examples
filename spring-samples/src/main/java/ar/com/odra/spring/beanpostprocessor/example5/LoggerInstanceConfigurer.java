/**
 * 
 */
package ar.com.odra.spring.beanpostprocessor.example5;

import java.lang.reflect.Field;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import ar.com.odra.spring.util.CoreUtil;

/**
 * 
 */
@Component
public class LoggerInstanceConfigurer implements BeanPostProcessor, Ordered {

	private static final Logger LOGGER = Logger.getLogger(LoggerInstanceConfigurer.class);

	/**
	 * @param bean
	 * @param beanClass
	 */
	private void injectLoggerInstance(Object bean, Class<?> beanClass) {
		LOGGER.debug("Bean [" + beanClass + "] processing in logger configurer.");
		Field[] fields = CoreUtil.getAllFieldWith(ar.com.odra.spring.beanpostprocessor.example5.annotation.Log.class, beanClass);
		for (Field field : fields) {
			field.setAccessible(true);
			if (Logger.class.isAssignableFrom(field.getType())) {
				LOGGER.debug("Creating logger for [" + field + "] in type [" + beanClass + "].");
				Logger logger = Logger.getLogger(field.getType());
				try {
					field.set(bean, logger);
				} catch (IllegalArgumentException e) {
					LOGGER.warn("Cannot set logger to property [" + field + "] in type [" + beanClass + "].", e);
				} catch (IllegalAccessException e) {
					LOGGER.warn("Cannot set logger to property [" + field + "] in type [" + beanClass + "].", e);
				}
			}
			field.setAccessible(false);
		}
		LOGGER.debug("Logger bean injected in type [" + beanClass + "] was processed.");
	}

	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

	public int getOrder() {
		return Ordered.HIGHEST_PRECEDENCE;
	}

	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		LOGGER.debug("Post processing bean instance [" + bean + "].");
		Object beanSource = CoreUtil.getSourceObject(bean);
		if(beanSource != null) {
			injectLoggerInstance(beanSource, beanSource.getClass());
		}
		return bean;
	}
}
