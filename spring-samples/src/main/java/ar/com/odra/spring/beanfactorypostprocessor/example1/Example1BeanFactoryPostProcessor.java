/**
 * 
 */
package ar.com.odra.spring.beanfactorypostprocessor.example1;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

/**
 * @author c.gusala
 * 
 */
@Component
public class Example1BeanFactoryPostProcessor implements BeanFactoryPostProcessor {

	private int beansSize = 0;
	
	private static Logger	LOGGER	= Logger.getLogger(Example1BeanFactoryPostProcessor.class);

	public void postProcessBeanFactory(ConfigurableListableBeanFactory factory) throws BeansException {
		LOGGER.info("The factory contains the followig beans:");
		String[] beanNames = factory.getBeanDefinitionNames();
		for ( int i = 0; i < beanNames.length; ++i ) {
			LOGGER.info(beanNames[i]);
			this.beansSize++;
		}
	}

	/**
	 * @return the beansSize
	 */
	public int getBeansSize() {
		return beansSize;
	}

	/**
	 * @param beansSize the beansSize to set
	 */
	public void setBeansSize(int beansSize) {
		this.beansSize = beansSize;
	}
}
