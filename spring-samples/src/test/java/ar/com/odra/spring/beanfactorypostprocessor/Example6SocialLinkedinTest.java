/**
 * 
 */
package ar.com.odra.spring.beanfactorypostprocessor;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.linkedin.api.LinkedIn;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "classpath:example6/app-context.xml" })
public class Example6SocialLinkedinTest extends OdraSpringBaseTest {

	/*@Autowired
	private ConnectionRepository connectionRepository;*/
	@Autowired
	private LinkedIn linkedIn;
	@Test
	public void testSocial_SpringLinkedinConnection() {
		assertNotNull("ConnectionRepository size must be more than 0.", this.linkedIn);
		assertNotNull("ConnectionRepository size must be more than 0.", this.linkedIn.isAuthorized());
	}
	

	
	
		//model.addAttribute("profile", connection.getApi().profileOperations().getUserProfile());
	
}
