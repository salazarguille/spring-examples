/**
 * 
 */
package ar.com.odra.spring.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.cglib.proxy.Enhancer;

/**
 * @author c.gusala
 * 
 */
public class CoreUtil {
	
	private static final Logger LOGGER = Logger.getLogger(CoreUtil.class);
	
	public static final String COMMA = ",";

	private CoreUtil(){
		super();
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getSourceObject(Object proxyInstance) {
		Class<?> beanClass = proxyInstance.getClass();
		Object beanSource = null;
		if (AopUtils.isJdkDynamicProxy(proxyInstance)) {
			try {
				beanSource = ((Advised) proxyInstance).getTargetSource().getTarget();
			} catch (Exception e) {
				LOGGER.warn("Cannot get source bean in jdk dynamic proxy [" + beanClass + "].", e);
			}
		} else {
			if(AopUtils.isCglibProxy(proxyInstance)) {
				try {
					beanSource = ((Advised) proxyInstance).getTargetSource().getTarget();
				} catch (Exception e) {
					LOGGER.warn("Cannot get source bean in cglib proxy [" + beanClass + "].", e);
				}
			} else {
				if(AopUtils.isAopProxy(proxyInstance)) {
					try {
						beanSource = ((Advised) proxyInstance).getTargetSource().getTarget();
					} catch (Exception e) {
						LOGGER.warn("Cannot get source bean in aop proxy [" + beanClass + "].", e);
					}
				} else {
					if (Proxy.isProxyClass(beanClass)) {
						// return (T) proxy; // expected to be cglib proxy then, which is simply a specialized class
						LOGGER.warn("Cannot get source bean in proxy [" + beanClass + "].");
					} else {
						beanSource = proxyInstance;
					}
				}
			}
		}
		return (T) beanSource;
	}
	
	public boolean isEnhancerType(Class<?> type) {
		return Enhancer.isEnhanced(type);
	}
	
	public static Field[] getFieldsDeclaredFor(final Class<?> clazz){
		return clazz.getDeclaredFields();
	}
	
	public static boolean hasAnnotation(Field field, Class<? extends Annotation> annotation){
		return field != null && field.isAnnotationPresent(annotation);
	}
	
	public static Field[] getAllFieldWith(final Class<? extends Annotation> annotation, final Class<?> clazz){
		Field[] fields = getFieldsDeclaredFor(clazz);
		List<Field> listFields = new ArrayList<Field>();
		for(Field f: fields){
			if(hasAnnotation(f, annotation)){
				listFields.add(f);
			}
		}
		Field[] a = new Field[listFields.size()];
		int i = 0;
		for(Field m : listFields){
			a[i] = m;
			i++;
		}
		return a;
	}
}
